FROM node:11.10.1 as builder
WORKDIR /app
COPY ./package.json ./
RUN npm install
COPY ./ ./
RUN npm run test:coverage
RUN npm run build

FROM nginx:alpine
EXPOSE 3000
RUN apk add --no-cache bash
WORKDIR /usr/share/nginx/html
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build .

ENTRYPOINT nginx -g 'daemon off;' 
